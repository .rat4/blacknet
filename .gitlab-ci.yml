#!/bin/false
#
# Copyright (c) 2020-2025 Pavel Vasin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

.gcc:
  image: gcc:14.2.0@sha256:329554b28adb201ffb3ecec415cf184958effa7c7e57d5272f60366f36a3f23c

.openjdk:
  image: maven:3.9.9-eclipse-temurin-21@sha256:b89ede2635fb8ebd9ba7a3f7d56140f2bd31337b8b0e9fa586b657ee003307a7
  variables:
    MAVEN: "mvn --batch-mode --no-transfer-progress" #TODO 4
    MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
  cache:
    paths:
      - .m2/repository

stages:
  - scan
  - test
  - build

gcc-test:
  extends: .gcc
  stage: test
  before_script:
    - echo "deb http://deb.debian.org/debian bookworm-backports main" >> /etc/apt/sources.list
    - apt-get update
    - apt-get -t bookworm-backports -y install meson
    - apt-get -y install libboost-dev libboost-random-dev libboost-test-dev
  script:
    - meson setup -Dbuildtype=plain -Dwarning_level=0 -Dtests_report_junit=true build
    - meson test --no-stdsplit --print-errorlogs -C build
  rules:
    - changes:
      - "**/meson*"
      - "**/*.h"
      - "**/*.cpp"
      when: on_success
    - when: never
  artifacts:
    reports:
      junit:
        - "build/TEST-*.xml"

openjdk-scan:
  extends: .openjdk
  stage: scan
  script:
    - $MAVEN artifact:check-buildplan cyclonedx:makeAggregateBom
  rules:
    - changes:
      - pom.xml
      - "**/pom.xml"
      when: on_success
    - when: never
  artifacts:
    reports:
      cyclonedx: target/bom.json

openjdk-test:
  extends: .openjdk
  stage: test
  script:
    - $MAVEN test
  rules:
    - changes:
      - pom.xml
      - "**/pom.xml"
      - "**/*.java"
      - "**/*.kt"
      when: on_success
    - when: never
  artifacts:
    reports:
      junit:
        - "**/target/surefire-reports/TEST-*.xml"

openjdk-build:
  extends: .openjdk
  stage: build
  variables:
    DESTDIR: "blacknet-$CI_COMMIT_TAG"
  script:
    - $MAVEN verify
    - mkdir -p $DESTDIR
    - cp -a daemon/target/appassembler/* $DESTDIR
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success
    - when: never
  artifacts:
    name: blacknet-gitlab-build
    paths:
      - $DESTDIR
