/*
 * Copyright (c) 2023 Pavel Vasin
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

package ninja.blacknet.window.main

import java.awt.event.ActionEvent
import java.awt.event.KeyEvent.*
import javax.swing.AbstractAction
import ninja.blacknet.swing.dsl.*
import ninja.blacknet.window.preferences.PreferencesDialog

class PreferencesAction : AbstractAction() {
    init {
        name = "Preferences"
        accelerator = KeyStroke(VK_P, CTRL_DOWN_MASK)
    }

    override fun actionPerformed(e: ActionEvent) {
        PreferencesDialog()
    }
}
